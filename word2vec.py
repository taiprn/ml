import numpy as np
from tqdm import tqdm
from os.path import isdir
from os import mkdir, listdir
import mmap
from os.path import splitext, join
import zipfile

glove_dir = "glove"

if isdir(glove_dir) is False:
    mkdir(glove_dir)

glove_files = ["glove.6B.300d.txt", "glove.42B.300d.txt", "glove.840B.300d.txt"]

embedding_vector_length = 300
embeddings_index = {}
word_index = {}


def get_num_lines(file_path):
    fp = open(file_path, "r+")
    buf = mmap.mmap(fp.fileno(), 0)
    lines = 0
    while buf.readline():
        lines += 1
    return lines


# wi = 1
glove_path = join(glove_dir, glove_files[0])

with open(glove_path, "r", encoding="utf8") as f:
    for line in tqdm(f, total=get_num_lines(glove_path)):
        values = line.split()
        word = values[0]
        coefs = np.asarray(values[1:], dtype='float32')
        embeddings_index[word] = coefs
    # word_index[word] = wi
    # wi += 1


print('Found %s word vectors.' % len(embeddings_index))


def len_embedding_vector():
    return embedding_vector_length
