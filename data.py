from xml.etree.ElementTree import parse
import numpy as np
from word2vec import embeddings_index, embedding_vector_length
from os.path import splitext
import re
from matplotlib import pyplot as plt
from tqdm import tqdm
from os.path import isdir, join
from os import mkdir, walk, listdir
import json
import io
import math


dataset_path = "datasets/eRisk 2018 - training/2017 train/"
path_test = "datasets/eRisk 2018 - training/2017 test/"
path_pos = join(dataset_path, "positive_examples_anonymous_chunks")
path_neg = join(dataset_path, "negative_examples_anonymous_chunks")
gt_path = join(dataset_path, "risk_golden_truth.txt")
test_gt_path = join(path_test, "test_golden_truth.txt")

POST_CONCAT = "concat"
POST_SINGLE = "single"

posts = POST_CONCAT


def seq2vec(wordvec, weighted_mean=False):
    seq2vec = wordvec.mean(axis=0)
    if weighted_mean is True:
        seq2vec *= wordvec.std(axis=0)
    return seq2vec


class Dataset:
    def __init__(self, **kwargs):
        with open(gt_path, "r") as f:
            lines = f.readlines()
        self.subjects_verdict = dict()
        for l in lines:
            v = l.rstrip().split()
            self.subjects_verdict[v[0]] = v[1]
        with open(test_gt_path, "r") as f:
            lines = f.readlines()
        self.subjects_verdict_test = dict()
        for l in lines:
            v = l.rstrip().split()
            if len(v) == 2:
                self.subjects_verdict_test[v[0]] = v[1]
        self.word_index = dict()

    def store_dataset(self):
        self.store(
            path_pos,
            folder="train_data",
            test=False)
        self.store(
            path_neg,
            folder="train_data",
            test=False)
        self.store(
            path_test,
            folder='test_data',
            test=True)

    def store(self, path, folder, test=False):
        if isdir(folder) is False:
            mkdir(folder)
        for path_root, dirs, files in walk(path):
            for file in tqdm(files):
                _, ext = splitext(file)
                if ext == '.xml':
                    filename = join(path_root, file)
                    subj = file.split("_")
                    chunk = subj[2].split('.')[0]
                    subj = subj[0] + '_' + subj[1]
                    if test is True:
                        verdict = self.subjects_verdict_test[subj]
                    else:
                        verdict = self.subjects_verdict[subj]
                    try:
                        tree = parse(filename)
                    except Exception as e:
                        print(filename)
                        print(e)
                        continue
                    # root = tree.getroot()
                    if posts == POST_CONCAT:
                        text = ""
                        for elem in tree.iter():
                            if elem.tag == "TEXT":
                                txt = elem.text
                                if len(txt.split()) > 0:
                                    text += elem.text + '\n'

                        fname = "_".join([subj, chunk, verdict])
                        fname += '.txt'

                        path = join(folder, fname)
                        with io.open(path, "w", encoding='utf-8') as f:
                            f.write(text)
                    elif posts == POST_SINGLE:
                        text = ""
                        post = 0
                        lname = [subj, chunk, str(post), verdict]
                        for elem in tree.iter():
                            if elem.tag == "TEXT":
                                txt = elem.text
                                if len(txt.split()) > 0:
                                    text = elem.text
                                    fname = "_".join(lname)
                                    fname += '.txt'
                                    post += 1
                                    lname[2] = str(post)
                                    path = join(folder, fname)
                                    with io.open(path, "w", encoding='utf-8') as f:
                                        f.write(text)

    # def words_to_sequence_trunc(self, words, seq_size):
    #     wlist = [self.word_index[w] for w in words if w in self.word_index]
    #     seq = np.asarray(wlist)
    #     pads = []
    #     for i in range(math.ceil(len(wlist) / seq_size)):
    #         max_size = min(seq_size, len(wlist))
    #         seq1 = seq[seq_size * i:seq_size * i + max_size]
    #         pad = np.zeros(seq_size)
    #         pad[:seq1.shape[0]] = seq1
    #         pads.append(pad)
    #         break
    #     return pads

    def words_to_sequence_trunc(self, words, seq_size):
        wlist = [self.word_index[w] for w in words if w in self.word_index]
        seq = np.asarray(wlist)
        max_size = min(seq_size, len(wlist))
        seq = seq[:max_size]
        pad = np.zeros(seq_size)
        pad[:seq.shape[0]] = seq
        return pad

    def store_dataset_vectors(self):
        path = 'train_data'
        path_vec = 'vectors'
        if isdir(path_vec) is False:
            mkdir(path_vec)
        lengths = []

        if posts == POST_CONCAT:
            exp = re.compile("train_subject\d+_\d+_(0|1)\.txt")
        else:
            exp = re.compile("train_subject\d+_\d+_\d+_(0|1)\.txt")

        print("Build word index.")
        wi = 1
        for file in tqdm(listdir(path)):
            filename = join(path, file)
            if exp.match(file):
                with io.open(filename, encoding='utf-8') as doc:
                    tokens = []
                    for line in doc:
                        # ^[\s\w\d\?><;,\{\}\[\]\-_\+=!@\#\$%^&\*\|\']*$
                        ln = re.sub("[^\w]", " ", line)
                        ln = ln.lower()
                        tokens.extend(ln.split())
                    for tok in tokens:
                        if tok not in self.word_index:
                            self.word_index[tok] = wi
                            wi += 1
                    lengths.append(len(tokens))

        with open("word_index.json", "w") as f:
            json.dump(self.word_index, f, indent=2)

        print("Save dataset vectors.")

        for file in tqdm(listdir(path)):
            filename = join(path, file)
            if exp.match(file):
                verd = int(file[-5])
                with io.open(filename, encoding='utf-8') as doc:
                    tokens = []
                    for line in doc:
                        ln = re.sub("[^\w]", " ", line)
                        ln = ln.lower()
                        tokens.extend(ln.split())
                    seq = self.words_to_sequence_trunc(tokens, 500)
                    fn = join(path_vec, file)
                    np.savez(fn, input=seq, verdict=verd)
                    # for i, s in enumerate(seq):
                    #     fn = join(path_vec, str(i) + file)
                    #     np.savez(fn, input=s, verdict=verd)

        len_wi = len(self.word_index) + 1
        emb_length = embedding_vector_length
        embedding_matrix = np.zeros((len_wi, emb_length))
        tmp = 0
        for word, i in self.word_index.items():
            embedding_vector = embeddings_index.get(word)
            if embedding_vector is not None:
                # words not found in embedding index will be all-zeros.
                tmp += 1
                embedding_matrix[i] = embedding_vector
        print("Found %d words in pretrained" % tmp)
        np.save("embedding_matrix.npy", embedding_matrix)

        plt.xlim([min(lengths), max(lengths)])
        plt.hist(lengths, bins=len(set(lengths)))
        plt.title('lengths')
        plt.xlabel('variable X')
        plt.ylabel('count')
        plt.show()


if __name__ == "__main__":
    d1 = Dataset()
    d1.store_dataset()
    d1.store_dataset_vectors()
