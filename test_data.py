import unittest
import json
from data import *

class Word2SeqTest(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(Word2SeqTest, self).__init__(*args, **kwargs)
        self.length = 5
        self.d = Dataset()
        with open("word_index.json", "r") as f:
            word_index = json.load(f)
        self.d.word_index = word_index
        
    def test_shape(self):
        self.assertTrue(
            len(self.d.words_to_sequence_trunc(["Mother","has","come","home"], self.length)) == self.length)
        
    def test_padding(self):
        vector = self.d.words_to_sequence_trunc(["Mother","has","come","home"], self.length)
        for i in range(self.length, len(vector)):
            self.assertTrue(vector[i] == 0.0)

            
        
unittest.main()
