import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.layers import LSTM, Flatten
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
from keras.optimizers import Adam
import os
import random
import re
import json
import datetime
import time


class RNNModel():

    def __init__(self, num_layers, batch_size,
                 learning_rate, embedding_vector_length,
                 input_length):

        with open("word_index.json", "r") as f:
            word_index = json.load(f)

        embedding_matrix = np.load("embedding_matrix.npy")

        self.num_layers = num_layers
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.vocab_size = len(word_index) + 1
        self.vector_len = embedding_vector_length
        self.embedding_matrix = embedding_matrix
        self.input_length = input_length

    def create_model(self):
        model = Sequential()
        model.add(Embedding(self.vocab_size,
                            self.vector_len,
                            weights=[self.embedding_matrix],
                            input_length=self.input_length,
                            trainable=False))
        model.add(Conv1D(filters=32, kernel_size=3, padding='same', activation='relu'))
        model.add(MaxPooling1D(pool_size=2))
        model.add(Conv1D(filters=32, kernel_size=3, padding='same', activation='relu'))
        model.add(MaxPooling1D(pool_size=2))
        # model.add(LSTM(300, return_sequences=True, activation='softmax'))
        # model.add(Dropout(0.2))
        model.add(LSTM(300, return_sequences=True))
        model.add(Conv1D(filters=32, kernel_size=3, padding='same', activation='relu'))
        model.add(MaxPooling1D(pool_size=2))
        model.add(LSTM(300))
        model.add(Dense(1, activation='sigmoid'))
        model.compile(loss='binary_crossentropy', optimizer='adam',
                      metrics=['accuracy'])
        # print(model.summary())
        return model

    def test():
        pass


# print((data["input"].item(-2)))
i = 0


def generate_input_train(b_size):
    global i
    names = names_train
    random.shuffle(names)
    length = int(len(names) / b_size)
    while 1:
        if (i + 1) * b_size > len(names):
            name_batch = names[i * b_size:]
        else:
            name_batch = names[i * b_size:(i + 1) * b_size]
        inputs, verdicts = [], np.array([])
        ts = len(name_batch)
        j = 0
        while j < ts:
            data = np.load(name_batch[j])
            verd = data["verdict"]
            inputs.append(data["input"])
            verdicts = np.append(verdicts, verd)
            j += 1
        if i >= length:
            i = 0
        else:
            i += 1
        yield np.asarray(inputs), verdicts


t = 0


def generate_input_test(b_size):
    global t
    names = names_valid
    length = int(len(names) / b_size)
    while 1:
        if (t + 1) * b_size > len(names):
            name_batch = names[t * b_size:]
        else:
            name_batch = names[t * b_size:(t + 1) * b_size]
        inputs, verdicts = [], np.array([])
        ts = len(name_batch)
        j = 0
        while j < ts:
            data = np.load(name_batch[j])
            verd = data["verdict"]
            inputs.append(data["input"])
            verdicts = np.append(verdicts, verd)
            j += 1
        if t >= length:
            t = 0
        else:
            t += 1
        yield np.asarray(inputs), verdicts
    t = 0

# print((data["input"].item(-2)))


if __name__ == "__main__":
    dt = datetime.datetime.fromtimestamp(time.time()).isoformat()
    dt = re.sub("(:|-)", "_", dt)
    print(dt)
    files = os.listdir('vectors')
    exp = re.compile("train_subject\d+_\d+_(0|1)\.txt")
    names = []
    for file in files:
        if exp.match(file):
            names.append(os.path.join('vectors', file))
    random.shuffle(names)
    # split = int(0.05 * len(names))
    # names = names[:split]
    # print(len(names))
    split = int(0.9 * len(names))
    names_train = names[:split]
    names_valid = names[split:]
    b_size = 128
    spe = len(names_train) / b_size
    val_steps = len(names_valid) / b_size
    # print(val_steps, len(names_valid), b_size)

    print("Creating the model.")

    model = RNNModel(num_layers=0, batch_size=b_size,
                     learning_rate=0, embedding_vector_length=300,
                     input_length=500).create_model()
    print("Running the model.")

    class_weight = {0: 1., 1: 6.}
    model.fit_generator(generate_input_train(b_size),
                        steps_per_epoch=spe, epochs=10,
                        validation_data=generate_input_test(b_size),
                        validation_steps=val_steps,
                        class_weight=class_weight)
    print("taip" + str(dt) + ".h5")
    model.save("taip" + str(dt) + ".h5")
