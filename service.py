from flask import Flask
from flask import render_template
from flask import request
from run import NeuralNetwork

app = Flask(__name__)

nn = NeuralNetwork()


@app.route("/", methods=['POST','GET'])
def index():
    global nn
    if request.method == 'GET':
        return render_template('index.html')
    elif request.method == 'POST':
        text = request.form['text']
        res = nn.predict(text)
        return render_template('index.html', verdict=res)


app.run()