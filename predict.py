import numpy as np
from keras.datasets import imdb
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
import zipfile
from io import BytesIO
import os, sys, re
import random
from model import RNNModel
from os.path import isdir, join
from os import mkdir, walk, listdir
from tqdm import tqdm
import json
import io


with open("word_index.json", "r") as f:
    word_index = json.load(f)


def words_to_sequence_trunc(words, seq_size):
    wlist = [word_index[w] for w in words if w in word_index]
    seq = np.asarray(wlist)
    max_size = min(seq_size, len(wlist))
    seq = seq[:max_size]
    pad = np.zeros(seq_size)
    pad[:seq.shape[0]] = seq
    return pad


path = "test_data"
x = []
y = []
xpos = []
xneg = []
ypos = []
yneg = []

for file in tqdm(listdir(path)):
    filename = join(path, file)
    exp = re.compile("test_subject\d+_\d+_(0|1)\.txt")
    if exp.match(file):
        with io.open(filename, encoding='utf-8') as doc:
            tokens = []
            for line in doc:
                ln = re.sub("[^\w]", " ", line)
                ln = ln.lower()
                tokens.extend(ln.split())
            vecs = words_to_sequence_trunc(tokens, 500)
            if vecs is not None:
                verd = int(file[-5])
                if verd == 1:
                    xpos.append(vecs)
                    ypos.append(verd)
                else:
                    xneg.append(vecs)
                    yneg.append(verd)
                x.append(vecs)
                y.append(verd)


# new model
model = RNNModel(num_layers=0, batch_size=512,
                 learning_rate=0, embedding_vector_length=300,
                 input_length=500).create_model()

model_h5 = sys.argv[1]
model.load_weights(model_h5)

# score = model.evaluate(np.asarray(x), np.asarray(y), batch_size=512, verbose=1)
# print('Test score:', score[0])
# print('Test accuracy:', score[1])


# score = model.evaluate(np.asarray(xpos), np.asarray(ypos),
#                        batch_size=512, verbose=1)
# print('Test score:', score[0])
# print('Test accuracy:', score[1])

# score = model.evaluate(np.asarray(xneg), np.asarray(yneg),
#                        batch_size=512, verbose=1)
# print('Test score:', score[0])
# print('Test accuracy:', score[1])
test_sbj = {}

chunks = [[] for i in range(11)]
print(len(chunks))

for file in tqdm(listdir(path)):
    filename = join(path, file)
    exp = re.compile("(test_subject\d+)_(\d+)_(0|1)\.txt")
    if exp.match(file):
        m = exp.match(file)
        subj = m.group(1)
        chunk = m.group(2)
        test_sbj[subj] = chunk

        with io.open(filename, encoding='utf-8') as doc:
            tokens = []
            for line in doc:
                ln = re.sub("[^\w]", " ", line)
                ln = ln.lower()
                tokens.extend(ln.split())
            vecs = words_to_sequence_trunc(tokens, 500)
            if vecs is not None:
                # print(int(chunk))
                chunks[int(chunk)].append((subj, vecs))

for i in range(1, 11):
    f = open(os.path.join('out', 'bit_' + str(i)), 'w')
    for j in tqdm(range(len(chunks[i]))):
        subj, vecs = chunks[i][j]
        y = model.predict(np.asarray([vecs]))
        pred = y[0][0]
        if pred > 0.5:
            code = 1
        else:
            code = 2
        str_out = "%s\t\t%s\n" % (subj, code)
        f.write(str_out)
        # print(str_out)
