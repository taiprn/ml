import unittest
import json
from data import *
from model import *

class ModelTest(unittest.TestCase):
    def test_b_size_train(self):
        b_size = 5
        x, y = next(generate_input_train(b_size))
        self.assertTrue(len(x)==b_size)
        self.assertTrue(len(y)==b_size)
        
    def test_b_size_test(self):
        b_size = 5
        x, y = next(generate_input_test(b_size))
        self.assertTrue(len(x)==b_size)
        self.assertTrue(len(y)==b_size)
        
    def test_model(self):
        num_layers = 3
        model = RNNModel(num_layers=num_layers, batch_size=5,
                     learning_rate=0, embedding_vector_length=300,
                     input_length=500).create_model()
        self.assertTrue(len(model.layers)==num_layers)
        

unittest.main()
