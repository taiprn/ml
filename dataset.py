from xml.etree.ElementTree import parse
from os.path import isdir, join
from os import listdir
import numpy as np
from word2vec import text_to_vec, seq2vec
import zipfile
from io import BytesIO


path = "vectors.zip"
zp = zipfile.ZipFile(path,'w')

class Loggable(type):
    @staticmethod
    def wrap(load):
        """Return a wrapped instance method"""

        def outer(self, path, verdict):
            return_value = load(self, path, verdict)
            print(return_value)
            return return_value
        return outer

    def __new__(cls, name, bases, attrs):
        print('new')
        """If the class has a 'load' method, wrap it"""
        if 'load' in attrs:
            attrs['load'] = cls.wrap(attrs['load'])
            print('wrapped')
        return super(Loggable, cls).__new__(cls, name, bases, attrs)


class Entry:
    def __init__(self, **kwargs):
        self.text = kwargs['TEXT'] if 'TEXT' in kwargs else None
        self.title = kwargs['TITLE'] if 'TITLE' in kwargs else None
        self.time = kwargs['DATE'] if 'DATE' in kwargs else None
        self.info = kwargs['INFO'] if 'INFO' in kwargs else None
        self.verdict = kwargs['VERDICT'] if 'VERDICT' in kwargs else None


class Dataset(metaclass=Loggable):
    def __init__(self, **kwargs):
        self.entries = []
        self.verdict = -1
        for key in kwargs:
            t = kwargs[key]
            path = t[0]
            verdict = t[1]
            print(path, verdict)
            self.load(path=path, verdict=verdict)

    def load(self, path, verdict):
        if isdir(path):
            oldlen = len(self.entries)
            for file in listdir(path)[:2]:
                self.load(join(path, file), verdict)
            return ('OK!', 'Done loading', path + ',' +
                    str(len(self.entries) - oldlen), 'new entries')
        try:
            tree = parse(path)
        except Exception as e:
            print(e)
            return (-1, 'Error loading' + path)
        root = tree.getroot()
        new_entries = []
        for elem in tree.iter():
            if elem.tag == "TEXT":
                # print("<><><>", elem.text.encode("utf-8"))
                new_entries.append(Entry(**{**{"TEXT": elem.text}, **{'VERDICT': verdict}}))
        for entry in new_entries:
            #if len(entry.text) > 100:
              #  entry.vector = text_to_vec(entry.text)[:100]
                #print(entry.vector)
            #else:
             #   vec = text_to_vec(entry.text)
            #    zeros = np.zeros((100))
             #   for i in range(len(vec)):
              #      zeros[i] = vec[i]
               # entry.vector = zeros
                #print(entry.vector)
            for entry in new_entries:
                vec = text_to_vec(entry.text)
                if len(vec) > 100:
                    vec = vec[:100]
                zeros = np.zeros((100))
                for i in range(len(vec)):
                    zeros[i] = vec[i]
                entry.vector = zeros
                #print(entry.vector)
            entry.seq2vec = 0  # seq2vec(entry.vector)
            #print(entry.vector)
			
        self.store(new_entries, path)

        return ('OK!', 'Done loading',
                path, ',' + str(len(new_entries)), 'new entries')

    def store(self, entries, path):	
        # modificat a. i. toate input-urile sa fie salvate cu un sg verdict	
        for i, entry in enumerate(entries):
            f = BytesIO()
            np.savez(f, input = entry.vector, verdict = entry.verdict)
            zp.writestr("vec"+path+str(i), f.getvalue())


def main():
    d1 = Dataset(
        neg=('D:\\Facultate\\Master\\TAIP\\reddit-training-ready-to-share\\negative_examples_anonymous', 0),
        pos=('D:\\Facultate\\Master\\TAIP\\reddit-training-ready-to-share\\positive_examples_anonymous', 1))	
    zp.close()


main()
