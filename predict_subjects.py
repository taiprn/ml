import numpy as np
from keras.datasets import imdb
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
import zipfile
from io import BytesIO
import os, sys, re
import random
from model import RNNModel
from os.path import isdir, join
from os import mkdir, walk, listdir
from tqdm import tqdm
import json
import io


with open("word_index.json", "r") as f:
    word_index = json.load(f)


def words_to_sequence_trunc(words, seq_size):
    wlist = [word_index[w] for w in words if w in word_index]
    seq = np.asarray(wlist)
    max_size = min(seq_size, len(wlist))
    seq = seq[:max_size]
    pad = np.zeros(seq_size)
    pad[:seq.shape[0]] = seq
    return pad


path = "test_data"
x = []
y = []
xpos = []
xneg = []
ypos = []
yneg = []

for file in tqdm(listdir(path)):
    filename = join(path, file)
    exp = re.compile("test_subject\d+_\d+_(0|1)\.txt")
    if exp.match(file):
        with io.open(filename, encoding='utf-8') as doc:
            tokens = []
            for line in doc:
                ln = re.sub("[^\w]", " ", line)
                ln = ln.lower()
                tokens.extend(ln.split())
            vecs = words_to_sequence_trunc(tokens, 500)
            if vecs is not None:
                verd = int(file[-5])
                if verd == 1:
                    xpos.append(vecs)
                    ypos.append(verd)
                else:
                    xneg.append(vecs)
                    yneg.append(verd)
                x.append(vecs)
                y.append(verd)


# new model
model = RNNModel(num_layers=0, batch_size=512,
                 learning_rate=0, embedding_vector_length=300,
                 input_length=500).create_model()

# model = RNNModel(max_comment_length=100, num_layers=0, batch_size=512,
#                      learning_rate=0, word_index=word_index, embedding_vecor_length=embedding_vector_length,
#                      embedding_matrix=embedding_matrix, max_input_length=100).create_model()
model_h5 = sys.argv[1]
model.load_weights(model_h5)
phrase = """My ex and I broke up over three years ago and initially the thought of not talking to her for a month was AGONIZING.

Fast forward, I spent numerous times breaking NC and trying to work things out over the years to no avail.

I would have a REALLY hard time staying strong when she initiated contact.  I'd say, "Well, what's the harm in engaging in conversation?"  It ultimately would lead to me being more hurt than before.

I can honestly say that now I'm finally ready to move on.  For so long I kept hoping for closure but **I realized that closure comes from within.**  There's nothing more for us to say to each other.

Previously, I would look at my NC counter nervously as it got above 20 days.  I'd dread the impending contact from her side.  Nowadays, I'm excited as the days pile up.  The momentum is great.  I say to myself, "If I can do a month, imagine how three months would feel?  Imagine how a year would feel??!?"

I'm sure a lot of you still have hope of getting back with your ex or a lot of you don't really want to never speak to them again.  I know everyone's situation is different but from my experience I can say that you're better off just sticking with NC.  I wasted 3 years going back and forth.  If I would have just stuck to my guns in the first year I'd be better off right now.

I'm proud of myself and I look forward to being even stronger and taking back control of my life.

*There is one small caveat in which I think it's okay to break NC.  If and only if the person makes it clear that they want to get back together AND it's what you want.  Any other small talk is pointless and will only set you back.*

MERRY CHRISTMAS! (if you celebrate that kinda thing)  I just wanted to share this with you guys.  Stay strong."""

#vec = text_to_vec(phrase)
#if len(vec) > 100:
#    vec = vec[:100]
#zeros = np.zeros((1, 100))
#for i in range(len(vec)):
#    zeros[0][i] = vec[i]
#zeros = zeros[np.newaxis, :].T
#print(model.predict(zeros))
#print(x)

for subj in range(x.shape[0]):
    

score = model.evaluate(np.asarray(x), np.asarray(y), batch_size=512, verbose=1)
print('Test score:', score[0])
print('Test accuracy:', score[1])


score = model.evaluate(np.asarray(xpos), np.asarray(ypos), batch_size=512, verbose=1)
print('Test score:', score[0])
print('Test accuracy:', score[1])

score = model.evaluate(np.asarray(xneg), np.asarray(yneg), batch_size=512, verbose=1)
print('Test score:', score[0])
print('Test accuracy:', score[1])
