import numpy as np
from keras.datasets import imdb
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
import zipfile
from io import BytesIO
import os, sys, re
import random
from model import RNNModel
from os.path import isdir, join
from os import mkdir, walk, listdir
from tqdm import tqdm
import json
import io
from meta_eval import Benchmarked


with open("word_index.json", "r") as f:
    word_index = json.load(f)


def words_to_sequence_trunc(words, seq_size):
    wlist = [word_index[w] for w in words if w in word_index]
    seq = np.asarray(wlist)
    max_size = min(seq_size, len(wlist))
    seq = seq[:max_size]
    pad = np.zeros(seq_size)
    pad[:seq.shape[0]] = seq
    return pad


class NeuralNetwork:
    def __init__(self):
        self.model = None
        self.init_model()

    @Benchmarked
    def init_model(self):
        self.model = RNNModel(num_layers=0, batch_size=512,
                     learning_rate=0, embedding_vector_length=300,
                     input_length=500).create_model()

        model_h5 = 'taip2018_01_06T16_49_15.236213.h5'
        self.model.load_weights(model_h5)

    @Benchmarked
    def predict(self, text):
        t = re.sub("[^\w]", " ", text)
        t = t.lower()
        t.split()
        vecs = words_to_sequence_trunc(t, 500)
        y = self.model.predict(np.asarray([vecs]))
        pred = y[0][0]
        if pred > 0.5:
            code = "depressive"
        else:
            code = "nondepressive"
        return code


from random import randint

from string import printable

if __name__ == "__main__":
    nn = NeuralNetwork()
    for i in range(2):
        nn.predict(''.join([printable[randint(0,len(printable)-1)] for n in range(randint(100, 500))]))
