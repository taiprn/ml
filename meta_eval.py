def MeasureTime(method):
    def timed(*args, **kw):
        from time import time
        ts = time()
        result = method(*args, **kw)
        te = time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print('%r  %2.2f ms' %
                  (method.__name__, (te - ts) * 1000))
        return result
    return timed


def Benchmarked(method):
    import cProfile
    import pstats
    from io import StringIO
    from os import linesep

    def measured(*args, **kw):

        pr = cProfile.Profile()

        pr.enable()
        result = method(*args, **kw)
        pr.disable()
        s = StringIO()
        sortby = 'cumulative'
        ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
        ps.print_stats()
        lines = s.getvalue().split('\n')
        print('\n'.join(lines[:min(len(lines), 15)]))
        return result
    return measured


@Benchmarked
def printare():
    from os import walk
    s = 0
    for r, d, f in walk('C:\\'):
        s += len([x for x in d + f])
    return s


print(printare())
